import numpy as np
from skimage.filters import gaussian
from skimage.transform import resize
from skimage.exposure import adjust_gamma

def gaussian_pyramid(img, n_layers, sigma):
    """Построение пирамиды Гаусса

    Вход: изображение, количество уровней, sigma
    Выход: уровни пирамиды

    """
    pyramid = [img]
    for _ in range(n_layers - 1):
        nxt = pyramid[-1]
        nxt = gaussian(nxt, sigma, channel_axis= (None if len(img.shape)==2 else -1))
        h, w = nxt.shape[:2]
        pyramid.append(nxt)
    return pyramid


def laplacian_pyramid(img, n_layers, sigma):
    """Построение пирамиды Лапласа

    Вход: изображение, количество уровней, sigma
    Выход: уровни пирамиды

    """
    pyramid_gauss = gaussian_pyramid(img, n_layers, sigma)
    pyramid = []
    for i in range(n_layers - 1):
        ths = pyramid_gauss[i]
        nxt = pyramid_gauss[i+1]
        pyramid.append(ths - nxt)
    pyramid.append(pyramid_gauss[-1])
    return pyramid


def blend_pyramids(pyramid_a, pyramid_b, pyramid_mask):
    """Смешивание пирамид

    Вход: пирамида Лапласа для первого изображения, пирамида Лапласа для второго изображения, пирамида Гаусса для маски
    Выход: «смешанная» пирамида

    """
    blend_pyramid = []
    for i in range(len(pyramid_a)):
        mask = pyramid_mask[i][:, :, None].repeat(repeats=3, axis=-1)
        
        a = pyramid_a[i]
        b = pyramid_b[i]

        masked_a = (1 - mask) * a
        masked_b = mask * b
        blend = masked_a + masked_b
        blend_pyramid.append(blend)
    return blend_pyramid


def blend_image(blend_pyramid):
    """Склейка изображений

    Вход: «смешанная» пирамида
    Выход: склеенное изображение

    """
    target_shape = blend_pyramid[0].shape
    img = 0
    for i in range(len(blend_pyramid)):
        level = blend_pyramid[i]
        img = img + level
    return img


def glue(object, object_mask, background, sigma=1, n_layers=10):
    gaussian_mask = gaussian_pyramid(gaussian(object_mask, sigma), n_layers, sigma)
    laplacian_background = laplacian_pyramid(background, n_layers, sigma)
    laplacian_object = laplacian_pyramid(object, n_layers, sigma)
    result_pyramid = blend_pyramids(laplacian_background, laplacian_object, gaussian_mask)
    pyramid_blend = blend_image(result_pyramid).clip(0, 1)
    
    return gaussian_mask, laplacian_background, laplacian_object, result_pyramid, pyramid_blend