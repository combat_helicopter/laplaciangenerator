import os
from typing import Iterable
from math import ceil

import uuid
from scipy.special import rel_entr
from albumentations import Rotate

from PIL import Image
import numpy as np

from glue_v2 import glue
from config_v2 import *
from croper_v2 import extract_crops


def get_random_crop(back, crop_size):
    r = np.random.randint(back.shape[0] - crop_size)
    c = np.random.randint(back.shape[1] - crop_size)
    
    crop = back[r:r+crop_size, c:c+crop_size]
    hist = np.concatenate([
        np.histogram(crop, bins=np.arange(0, 256), density=True)[0], 
        np.histogram(crop, bins=np.arange(0, 256), density=True)[0],
        np.histogram(crop, bins=np.arange(0, 256), density=True)[0]
    ]) / 3
    hist = np.clip(hist, a_min=1e-10, a_max=np.inf)
    return r, c, hist


def psi(shape):
    eps = 1e-3
    rgrid = 2 * np.arange(1, shape[0] + 1) / (shape[0] + 1)
    
    rf = np.e**(-1 / rgrid)
    irf = np.e**(-1 / (1 - rgrid + eps))
    rf = rf / (rf + irf)
    rf[shape[0] // 2:] = rf[:shape[0] - shape[0] // 2][::-1]


    cgrid = 2 * np.arange(1, shape[1] + 1) / (shape[1] + 1)
    
    cf = np.e**(-1 / cgrid)
    icf = np.e**(-1 / (1 - cgrid + eps))
    cf = cf / (cf + icf)
    cf[shape[1] // 2:] = cf[:shape[1] - shape[1] // 2][::-1]
    
    return rf[:, None] * cf[None, :]


def make_one_applique(back, back_mask, crops, class_number, crop_size_for_hist_extraction):
    if 0 == len(crops[class_number]):
        return back, back_mask

    r, c, hist = get_random_crop(back, crop_size_for_hist_extraction)

    entrs = np.array([
        np.sum(rel_entr(np.clip(crop_info['hist'], a_min=1e-10, a_max=np.inf), hist))
        for crop_info in crops[class_number]
    ])

    similarity = (1 / entrs)**3
    similarity = similarity / np.sum(similarity)
    
    id_of_sampled_object = np.random.choice(
       np.arange(len(entrs)), p=similarity
    )
    
    # id_of_sampled_object = np.random.choice(
    #    np.arange(len(crops[class_number]))
    # )
    
    crop, crop_mask = Rotate(limit=[-90, 90], p=1)(
        image=crops[class_number][id_of_sampled_object]['image_crop'],
        mask= crops[class_number][id_of_sampled_object]['mask_crop' ]
    ).values()

    crop_mask = crop_mask * (psi(crop_mask.shape)**0.1 > 0.5)

    crop_back = back[r: r+crop.shape[0], c: c+crop.shape[1]]
    crop_mask = crop_mask[:crop_back.shape[0], :crop_back.shape[1]]
    crop = crop[:crop_back.shape[0], :crop_back.shape[1]]

    _, _, _, _, glued_crop = glue(crop / 255, crop_mask / 255, crop_back / 255)
    glued_crop = (glued_crop * 255).astype(np.uint8)

    back[r: r+crop.shape[0], c: c+crop.shape[1]] = glued_crop
    back_mask[r: r+crop.shape[0], c: c+crop.shape[1]] = np.maximum(
        back_mask[r: r+crop.shape[0], c: c+crop.shape[1]], 
        crop_mask
    )

    return back, back_mask


def make_image_applique(back, back_masks_dict, crops, iters_in_class, crop_size_for_hist_extraction):
    global CLASSES

    for class_number in CLASSES:
        for i in range(iters_in_class[class_number]):
            back, back_masks_dict[class_number] = make_one_applique(
                back, back_masks_dict[class_number], 
                crops, class_number, crop_size_for_hist_extraction
            )

    return back, back_masks_dict


def generate(
    extraction_info: Iterable[tuple[str, dict[int, str]]],
    backs_info: dict[str, dict[int, str]],

    save_path: str,
    generated_objects_number: int,
    iters_in_class: dict[int, int],

    crop_size_for_hist_extraction, 
    th
):
    global CLASSES, SUFFIXES

    backs_pathes = list(backs_info.keys())
    crops = extract_crops(extraction_info, th)
    
    for i in range(generated_objects_number):
        back_path = np.random.choice(backs_pathes)
        back_masks_path = backs_info[back_path]

        back = np.array(Image.open(back_path))
        back_masks = {
            c:
            np.array(Image.open(back_masks_path[c]))
            if c in back_masks_path else
            np.zeros(shape=back.shape[:2], dtype=back.dtype)
            for c in CLASSES
        }

        back, back_masks_dict = make_image_applique(back, back_masks, crops, iters_in_class, crop_size_for_hist_extraction)

        back_name = os.path.basename(back_path).split('.')[0]
        name = f'{back_name}_' + str(uuid.uuid4())
        dir_path = os.path.join(save_path, name)
        os.makedirs(dir_path, mode=0o777, exist_ok=True)

        Image.fromarray(back).save(os.path.join(dir_path, 'image.jpeg'), mode='RGB')
        for c, m in back_masks_dict.items():
            Image.fromarray(m).save(os.path.join(dir_path, f'class_{c}.png'), mode='L')