SUFFIXES = ['jpeg']

NAMES_OF_CLASSES = [
    'Железо', 
    'Рыболовные снасти', 
    'Пластик', 
    'Дерево', 
    'Бетон', 
    'Резина'
]

CLASSES = list(range(len(NAMES_OF_CLASSES)))

CLASSES_MAP = {
    nc: i for i, nc in enumerate(NAMES_OF_CLASSES)
}

INVERSE_CLASSES_MAP = {
    v: k for k, v in CLASSES_MAP.items()
}