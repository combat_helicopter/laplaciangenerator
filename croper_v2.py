import os

import cv2
import numpy as np

from typing import Iterable
from PIL import Image

from config_v2 import *


def get_crop(
    canvas: np.ndarray[np.ndarray[np.uint8]], 
    row_tup: tuple[int, int], col_tup: tuple[int, int], 
    changer=None
) -> np.ndarray[np.ndarray[np.uint8]]:
    crop = canvas[row_tup[0]:row_tup[1], col_tup[0]:col_tup[1]]
    crop = changer(crop) if changer is not None else crop
    
    return crop


def extract_crops_from_mask(
    mask: np.ndarray[np.ndarray[np.uint8]], 
    th: float, changer=None
) -> list[dict]:
    mask_crops = []
    
    binary_mask = (mask > int(255 * th)).astype(np.uint8)
    components_number, labels, stats, centroids = cv2.connectedComponentsWithStats(binary_mask, cv2.CV_32S)
    
    # import matplotlib.pyplot as plt
    # plt.imshow(labels)
    # plt.show()

    for label, (x_left, y_up, x_len, y_len, pixels) in enumerate(stats):

        # import matplotlib.pyplot as plt
        # plt.imshow()

        if pixels < 10 * 10:
            continue
        if pixels > mask.shape[0] // 2 * mask.shape[1] // 2:
            continue

        c_row, c_col = y_up + y_len // 2, x_left + x_len // 2
        s_len = max(y_len, x_len)
        y_up, x_left = c_row - s_len // 2, c_col - s_len // 2

        row_tup = (
            max(0, y_up - int(s_len * 0.1)),
            min(mask.shape[0], y_up + int(s_len * 1.1))
        )
        col_tup = (
            max(0, x_left - int(s_len * 0.1)),
            min(mask.shape[1], x_left + int(s_len * 1.1))
        )

        # mask_crop = get_crop(mask, row_tup, col_tup, changer=changer)
        mask_crop = get_crop(labels, row_tup, col_tup, changer=changer) 
        mask_crop = ((mask_crop == label) * 255).astype(np.uint8)

        # print('label: ', label)
        # plt.imshow(mask_crop)
        # plt.show()

        mask_crops.append({'mask_crop': mask_crop, 'row_tup': row_tup, 'col_tup': col_tup})
    return mask_crops


def extract_crops_from_image(
    image: np.ndarray[np.ndarray[np.ndarray[np.uint8]]], 
    masks: dict[int, np.ndarray[np.ndarray[np.uint8]]], 
    th: float=0.2, changer=None
) -> dict[int, list[dict]]:
    image_extract = {}
    for c, mask in masks.items():
        mask_crops = extract_crops_from_mask(mask, th, changer)
        
        for i in range(len(mask_crops)):
            mask_crops[i]['image_crop'] = get_crop(image, mask_crops[i]['row_tup'], mask_crops[i]['col_tup'])
            
            img = mask_crops[i]['image_crop']
            msk = mask_crops[i]['mask_crop']
            color_hist = np.concatenate([
                np.histogram(img[:, :, 0][msk < 50], bins=np.arange(0, 256), density=True)[0], 
                np.histogram(img[:, :, 1][msk < 50], bins=np.arange(0, 256), density=True)[0],
                np.histogram(img[:, :, 2][msk < 50], bins=np.arange(0, 256), density=True)[0]
            ]) / 3
            
            mask_crops[i]['hist'] = color_hist
        
        image_extract[c] = mask_crops
        
    return image_extract


def extract_crops(pathes_iterator: Iterable[tuple[str, dict[int, str]]], th=0.2):
    global CLASSES

    crops = {c: [] for c in CLASSES}
    for i, (ip, mpd) in enumerate(pathes_iterator):
        image = np.asarray(Image.open(ip))
        masks = {c: np.asarray(Image.open(p)) for c, p in mpd.items()}
        
        extracted_crops = extract_crops_from_image(image, masks, th)
        
        for c, list_of_crops in extracted_crops.items():
            crops[c] += list_of_crops
    
    return crops